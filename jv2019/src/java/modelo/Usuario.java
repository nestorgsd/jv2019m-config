/** 
 * Proyecto: Juego de la vida.
 *  Implementa el concepto de Usuario según el modelo 1.2
 *  @since: prototipo 0.1.0
 *  @source: Usuario.java 
 *  @version: 0.2.0 - 2020/02/25
 *  @author AlbertoGonzalez - Grupo 5
 */

package modelo;

import util.Fecha;
import util.Formato;

public class Usuario extends Persona implements Identificable {

	public enum  RolUsuario {
		NORMAL,
		ADMIN,
		INVITADO;
	}

	private String id;
	private Fecha fechaAlta;
	private ClaveAcceso claveAcceso;
	private RolUsuario rol;
	
	
	/**
	 * Constructor convencional. Utiliza métodos set...()
	 * @param nif
	 * @param nombre
	 * @param apellidos
	 * @param domicilio
	 * @param correo
	 * @param fechaNacimiento
	 * @param fechaAlta
	 * @param claveAcceso
	 * @param rol
	 */
	public Usuario(Nif nif, String nombre, String apellidos,
			DireccionPostal domicilio, Correo correo, Fecha fechaNacimiento,
			Fecha fechaAlta, ClaveAcceso claveAcceso, RolUsuario rol) {
		super(nif, nombre, apellidos, domicilio, correo, fechaNacimiento);
		setFechaAlta(fechaAlta);
		setClaveAcceso(claveAcceso);
		setRol(rol);
		generarId();
	}

	private void generarId() {
		assert this.nif != null;
		assert this.nombre != null;
		assert this.apellidos != null;
		String[] apellidos = this.apellidos.split("[ ]");
		this.id =  ""+ this.nombre.charAt(0) 
				+ apellidos[0].charAt(0) + apellidos[1].charAt(0)
				+ this.nif.getTexto().substring(7);
		this.id = this.id.toUpperCase();
	}

	public void VariarId() {
		String alfabetoNif = Formato.LETRAS_NIF;
		String alfabetoNifDesplazado = alfabetoNif.substring(1) + alfabetoNif.charAt(0);
		this.id = this.id.substring(0, 4) 
				+ alfabetoNifDesplazado.charAt(alfabetoNif.indexOf(id.charAt(4)));
	}
	
	/**
	 * Constructor por defecto. Reenvía al constructor convencional.
	 * @throws ModeloException 
	 */
	public Usuario() {
		this(new Nif(), 
				"Invitado", 
				"Invitado Invitado", 
				new DireccionPostal(),
				new Correo(), 
				new Fecha().addYears(-16), 
				new Fecha(), 
				new ClaveAcceso(), 
				RolUsuario.INVITADO);
	}

	/**
	 * Constructor copia.
	 * @param usr 
	 */
	public Usuario(Usuario usuario) {
		this(new Nif(usuario.nif),
				new String(usuario.nombre),
				new String(usuario.apellidos),
				new DireccionPostal(usuario.domicilio),
				new Correo(usuario.correo),
				new Fecha(usuario.fechaNacimiento),
				new Fecha(usuario.fechaAlta),
				new ClaveAcceso(usuario.claveAcceso),
				usuario.rol
			);
	}

	@Override
	public String getId() {
		return id;
	}
	
	public Fecha getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Fecha fechaAlta) {
		if (fechaAltaValida(fechaAlta)) {
			this.fechaAlta = fechaAlta;
			return;
		}
		throw new ModeloException("Usuario.fechaAlta: No válida...");
	}

	/**
	 * Comprueba validez de una fecha de alta.
	 * @param fechaAlta.
	 * @return true si cumple.
	 */
	private boolean fechaAltaValida(Fecha fechaAlta) {
		return fechaAlta != null && !fechaAlta.after(new Fecha()); 			// Que no sea futura.
	}

	public ClaveAcceso getClaveAcceso() {
		return claveAcceso;
	}

	public void setClaveAcceso(ClaveAcceso claveAcceso) {
		if (claveAcceso != null) {
			this.claveAcceso = claveAcceso;
			return;
		}
		throw new ModeloException("Usuario.claveAcceso: null...");
	}

	public RolUsuario getRol() {
		return rol;
	}

	public void setRol(RolUsuario rol) {
		if (rol != null) {
			this.rol = rol;
			return;
		}
		throw new ModeloException("Usuario.rol: null...");
	}

	/**
	 * Redefine el método heredado de la clase Objecto.
	 * @return el texto formateado del estado -valores de atributos- de objeto de la clase Usuario.  
	 */
	@Override
	public String toString() {
		return super.toString() + String.format(
				  "%-16s %s\n"
				+ "%-16s %s\n"
				+ "%-16s %s\n"
				+ "%-16s %s\n", 
				"id:", this.id,
				"fechaAlta:", (this.fechaAlta.getYear()) + "." 
						+ (this.fechaAlta.getMes()) + "." 
						+ this.fechaAlta.getDia(), 
				"claveAcceso:", this.claveAcceso, 
				"rol:", this.rol
		);		
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((claveAcceso == null) ? 0 : claveAcceso.hashCode());
		result = prime * result + ((fechaAlta == null) ? 0 : fechaAlta.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((rol == null) ? 0 : rol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (claveAcceso == null) {
			if (other.claveAcceso != null)
				return false;
		} else if (!claveAcceso.equals(other.claveAcceso))
			return false;
		if (fechaAlta == null) {
			if (other.fechaAlta != null)
				return false;
		} else if (!fechaAlta.equals(other.fechaAlta))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (rol != other.rol)
			return false;
		return true;
	}

	@Override
	public Usuario clone() {
			return new Usuario(this);
	}
} 

